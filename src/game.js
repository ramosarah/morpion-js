export const PLAYER1 = Symbol('player1');
export const PLAYER2 = Symbol('player2');



//coups gagnants possibles
const WIN_PATTERNS = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],

    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],

    [0, 4, 8],
    [2, 4, 6]
];

export class Game {

    constructor() {
        this.board = Array(9).fill(null);
        this.currentPlayer = PLAYER1;
        this.movesCount = 0;
    }


    /**
    * Tente de jouer le coup indiqué pour le joueur courant.
    *
    * Retourn `true` si le coup a pu être joué. Sinon, retourne `false`.
    *
    * @param cell Le numéro de la case où jouer le coup.
    * @returns {boolean} La réussite ou l’échec du coup.
    */
    move(cell) {
        //si le joueur est NULL ou si la case n'est pas NULL le coup ne peut pas être joué
        if (this.currentPlayer === null || this.board[cell] !== null) {
            //on retourne FALSE et rien d'autre
            return false;
        }
        this.movesCount++;
        this.board[cell] = this.currentPlayer;
        return true;

    }


    /**
    * Vérifie si le joueur courant a réaliser une condition de victoire.
    *
    * En cas de victoire, retourne la liste des cases gagnantes.
    * Sinon retourne `false`.
    *
    * @returns {boolean|array}
    Coup gagnant, sinon `false`.
    */
    checkWin() {
        for (let pattern of WIN_PATTERNS) {
            let match = true;

            for (let cell of pattern) {
                if (this.board[cell] !== this.currentPlayer) {
                    match = false;
                    break;
                }
            }
            if (match) {
                return pattern;
            }
        }
        return false;
    }

    terminate() {
        this.currentPlayer = null;
    }

    isTerminates() {
        return this.currentPlayer === null;

    }




    switchPlayer() {
        if (this.movesCount === 9) {
            this.terminate();
            return;
        }

        switch (this.currentPlayer) {
            case PLAYER1 :
                this.currentPlayer = PLAYER2;
                break;

            case PLAYER2 :
                this.currentPlayer = PLAYER1;
                break;

            case null :
                break;
        }
    }



}
