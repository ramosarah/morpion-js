import {Game, PLAYER1} from './game';

//interface graphique
export class Board {
    /**
    * Affiche un plateau de jeu interactif dans l’élément spécifié.
    *
    * @param root
    Élément racine
    */

    constructor(root) {

        this.root = root;

        const board = document.createElement('div');
        board.classList.add('board');
        this.root.appendChild(board);

        // On crée un `div` qui représente la grille de jeu et qui contiendra les cases
        const grid = document.createElement('div');
        grid.classList.add('grid');
        board.appendChild(grid);


        //on crée les cases et on les ajoutes à la grid
        this.cells = [];
        for (let i = 0; i < 9; i++) {
            const cell = document.createElement('div');
            cell.classList.add('cell');

            const symbol = document.createElement('div');
            symbol.classList.add('symbol');
            cell.appendChild(symbol);

            this.cells.push(cell);
            grid.appendChild(cell);
        }


        for (let index of this.cells.keys()) {
            this.cells[index].onclick = () => this.onClickCell(index);
        }

        this.morpion = new Game();
        this.nom = [];
        this.nom['player 1'] = window.prompt("Quel est le nom du premier joueur ?");
        this.nom['player 2']  = window.prompt("Quel est le nom du deuxième joueur ?");


        let button = document.createElement('button');
        button.innerHTML = 'rejouer';
        button.onclick = () => this.rejouer();
        document.body.appendChild(button);

    }




    /**
    * Gère les cliques des joueurs sur les cases.
    *
    * @param index
    Indice de la case cliquée
    */
    onClickCell(index) {
        const cell = this.cells[index];

        // Si le coup n’est pas possible, on quitte la fonction sans rien faire de plus.
        if (!this.morpion.move(index)) {
            return;
        }

        // On indique que le joueur courant à jouer un coup dans la case cliquée en ajoutant la classe CSS adéquate.
        if (this.morpion.currentPlayer === PLAYER1) {
            cell.classList.add('player1');
        }else{
            cell.classList.add('player2');
        }

        let winPattern = this.morpion.checkWin();
        if (!winPattern) {
            // On indique que c’est l’autre joueur qui joue à présent.
            this.morpion.switchPlayer();
        }else {
            if(this.morpion.currentPlayer === PLAYER1) {
                window.alert("La partie est terminée, bravo "+this.nom['player 1'] );
            }else if (this.morpion.currentPlayer != PLAYER1) {
                window.alert("La partie est terminée, bravo "+this.nom['player 2']);
            }else{
                window.alert("Match null!");
            }
            this.morpion.terminate();
            this.highlight(winPattern);
        }
    }




    /**
    * Met en évidence les cases indiquées.
    *
    * @param indexes
    Liste des cases à mettre en évidence
    */
    highlight(indexes) {
        for (let index of indexes) {
            this.cells[index].classList.add('win');
        }


    }



    rejouer() {
        return document.location.reload();
    }




}
